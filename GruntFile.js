module.exports = function(grunt) {
  // Do grunt-related things in here

grunt.initConfig({
  sass: {
    dist: {
      files: {
        'main.css': 'main.scss'
      }
    }
  }
});
    
grunt.loadNpmTasks('grunt-contrib-sass');

grunt.registerTask('default', ['sass']);   
};

